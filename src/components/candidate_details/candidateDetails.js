import React from 'react';
import './candidateDetails.css';
import DetailsList from './details_list/detailsList';
import TagsList from './tags_list/tagsList';
import FileCopy from '../../assets/images/file-copy.svg';
import Mail from '../../assets/images/mail.svg';
import Share from '../../assets/images/share.svg';
import CalendarDate from '../../assets/images/date.svg';
import ThumbsDown from '../../assets/images/thumbs-down.svg';
import More from '../../assets/images/more.svg';

function CandidateDetails({ data }) {
    return (
        <div>
            <div className="selector">
                <input type="checkbox" className="selector-check" />
            </div>
            <div className="container-main">
                <div className="highlight">
                    <div className="icon">{data.avatar}</div>
                    <div className="info">
                        <div className="name">
                            {data.name}
                            <div className="social" />
                            <div className="social" />
                            <div className="social" />
                            <div className="social" />
                        </div>
                        <p className="designation">{data.designation}</p>
                    </div>

                    <div className="copy">
                        <img src={FileCopy} alt="copy" />
                    </div>
                </div>
                <div className="details">
                    <div className="top">
                        <div className="job">
                            <div>
                                Job
                                <div className="drop">
                                    {data.jobApplied}
                                    <span>&#9660;</span>
                                </div>
                            </div>
                        </div>
                        <div className="stage">
                            <div>
                                Current Stage
                                <div className="drop">
                                    {data.currentStage}
                                    <span>&#9660;</span>
                                </div>
                            </div>
                        </div>
                        <div className="options">
                            <img src={Mail} alt="mail" />
                            <img src={Share} alt="share" />
                            <img src={CalendarDate} alt="date" />
                            <img src={ThumbsDown} alt="thumbs down" />
                            <img src={More} alt="more" />
                        </div>
                    </div>
                    <div className="middle">
                        <div className="cards active-card">
                            {
                                data.details.map((detail) => {
                                    return (
                                        <DetailsList detail={detail} />
                                    );
                                })
                            }
                        </div>
                    </div>
                    <div className="bottom">
                        <div className="activities">
                            <div className="information">
                                <span>{data.noOfActivities} Activities</span> since last visit | Uploaded <span>{data.lastUploaded} days ago</span>
                            </div>
                            <div className="tags">
                                <div className="item">
                                    +
                                </div>
                                {data.tags.map((tag) => {
                                    return (
                                        <TagsList tag={tag} />
                                    );
                                })
                                }
                                <div className="item ex">
                                    + 3 more
                                </div>
                            </div>
                        </div>
                    </div>

                </div>
            </div>
        </div>
    );
}

export default CandidateDetails;
