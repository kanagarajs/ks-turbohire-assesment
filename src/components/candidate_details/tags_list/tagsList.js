import React from 'react';
import parse from 'html-react-parser';

function TagsList({ tag }) {
    return (
        <div className="item">
            {parse(tag)}
        </div>
    );
}

export default TagsList;
