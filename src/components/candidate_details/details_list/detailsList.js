import React, { useState } from 'react';
import parse from 'html-react-parser';
import Pending from '../../../assets/images/pending.svg';
import Close from '../../../assets/images/close.svg';

function DetailsList({ detail }) {
    const [showDetailedDescription, setShowDetailedDescription] = useState(false)
    return (
        <>
            {!showDetailedDescription ?
                <div className="item" onClick={()=>setShowDetailedDescription(true)}>
                    <p className="heading">
                        {detail.name}
                    </p>
                    <p className="detail">
                        {parse(detail.description)}
                    </p>
                    {
                        detail.name === 'One-way Interview' ? <img src={Pending} alt="pending" /> : null}

                    <p className="info">
                        {detail.additionalInformation}
                    </p>
                </div>
                :
                <div className="item-lg" onClick={()=>setShowDetailedDescription(false)}>
                    <div className="header">
                        <p className="heading">
                            {detail.name}
                        </p>
                        <img src={Close} className="close" alt="close" />
                    </div>
                    <div className="details">
                        <p>
                            Shown here are sample dummy content description for <strong>{detail.name}</strong>.
                        </p>
                        <p>
                            Lorem ipsum dolor sit amet, consectetur adipiscing elit. Pellentesque tortor nisl, suscipit at semper id, ultrices quis purus. Curabitur fermentum venenatis varius. Curabitur hendrerit ultricies lacus, eget tristique felis euismod et. Fusce urna dolor, congue a tempus id, bibendum ut est.
                        </p>
                        <p>
                            Lorem ipsum dolor sit amet, consectetur adipiscing elit. Pellentesque tortor nisl, suscipit at semper id, ultrices quis purus. Curabitur fermentum venenatis varius. Curabitur hendrerit ultricies lacus, eget tristique felis euismod et. Fusce urna dolor, congue a tempus id, bibendum ut est.
                        </p>
                    </div>

                </div>
            }
        </>
    );
}

export default DetailsList;
