import './App.css';
import CandidateDetails from './components/candidate_details/candidateDetails';

function App() {
  // Data
  const data = [
    {
      avatar: 'KS',
      name: 'Kanagaraj Subramaniam',
      designation: 'Sr. Web Developer, Microsoft India Pvt Ltd',
      jobApplied: 'Product Development (T-12345)',
      currentStage: 'Pool',
      details: [
        {
          name: 'One-way Interview',
          description: 'Video Requested',
          additionalInformation: 'by Aman Gour a minute ago'
        },
        {
          name: 'Custom Fields',
          description: 'Location - Mumbai <br/>CGPA 8.2 <br/><span>3 more</span>',
          additionalInformation: 'updated 5 hrs ago'
        },
        {
          name: 'Evaluation',
          description: 'Not evaluated for this stage',
          additionalInformation: '3 evaluations submitted'
        },
        {
          name: 'Education',
          description: 'Post Graduation in Natural Sciences from Teir 1',
          additionalInformation: '2 Candidates from same school hired previously'
        },
        {
          name: 'Experience',
          description: '5 years and 10 months',
          additionalInformation: '2 years 6 months of relevant experience from similar company'
        }
      ],
      noOfActivities: '2',
      lastUploaded: '8',
      tags: ['TH Tags', 'TP Tags', 'Company Tags &nbsp;&nbsp;X']
    },
    {
      avatar: 'GT',
      name: 'Gowtham Thangavelu',
      designation: 'Associate Software Engineer, Bosch Global Software Solutions',
      jobApplied: 'Software Development (T-23378)',
      currentStage: 'Aptitude',
      details: [
        {
          name: 'One-way Interview',
          description: 'Written Test Requested',
          additionalInformation: 'by Naveen Singh a minute ago'
        },
        {
          name: 'Custom Fields',
          description: 'Location - Coimbatore <br/>CGPA 9 <br/><span>5 more</span>',
          additionalInformation: 'updated 5 hrs ago'
        },
        {
          name: 'Evaluation',
          description: 'Not evaluated for this stage',
          additionalInformation: '5 evaluations submitted'
        },
        {
          name: 'Education',
          description: 'Under Graduation in Information Technology from Teir 2',
          additionalInformation: '10 Candidates from same school hired previously'
        },
        {
          name: 'Experience',
          description: '1 years and 11 months',
          additionalInformation: '8 months of relevant experience from similar company'
        }
      ],
      noOfActivities: '4',
      lastUploaded: '5',
      tags: ['KT Tags', 'Associate Tags &nbsp;&nbsp;X']
    }
  ];

  return (
    <div className="App">
      <nav>
        KS TurboHire Assessment
      </nav>
      <header className="App-header">
        {
          data.map((d) => {
            return (
              <CandidateDetails data={d} />
            );
          })
        }
      </header>
      <footer>
        <a href="https://www.turbohire.co/">www.turbohire.co</a>
      </footer>
    </div>
  );
}

export default App;
